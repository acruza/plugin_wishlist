'use strict';

var server = require('server');
server.extend(module.superModule);

var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var PAGE_SIZE_ITEMS = 15;

server.replace('Show', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var WishlistModel = require('*/cartridge/models/productList');
    var userName = '';
    var firstName;
    var rememberMe = false;
    if (req.currentCustomer.credentials) {
        rememberMe = true;
        userName = req.currentCustomer.credentials.username;
    }
    var loggedIn = req.currentCustomer.profile;

    var target = req.querystring.rurl || 1;
    var actionUrl = URLUtils.url('Account-Login');
    var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();
    var navTabValue = req.querystring.action;
    var breadcrumbs = [
        {
            htmlValue: Resource.msg('essilor.global.home', 'common', null),
            url: URLUtils.home().toString()
        }
    ];
    if (loggedIn) {
        firstName = req.currentCustomer.profile.firstName;
        breadcrumbs.push({
            htmlValue: Resource.msg('essilor.page.title.myaccount', 'account', null),
            url: URLUtils.url('Account-Show').toString()
        });
    }
    breadcrumbs.push({
        htmlValue: Resource.msg('essilor.page.title.wishlist', 'account', null),
        url: URLUtils.url('Wishlist-Show').toString()
    });
    var profileForm = server.forms.getForm('profile');
    profileForm.clear();
    var wishlistModel = new WishlistModel(
        list,
        {
            type: 'wishlist',
            publicView: true,
            pageSize: PAGE_SIZE_ITEMS,
            pageNumber: 1
        }
    ).productList;
    res.render('/wishlist/wishlistLanding', {
        wishlist: wishlistModel,
        navTabValue: navTabValue || 'login',
        rememberMe: rememberMe,
        userName: userName,
        actionUrl: actionUrl,
        profileForm: profileForm,
        breadcrumbs: breadcrumbs,
        oAuthReentryEndpoint: 1,
        loggedIn: loggedIn,
        firstName: firstName,
        socialLinks: loggedIn,
        publicOption: loggedIn,
        createAccountUrl: createAccountUrl
    });
    next();
});

server.get('GetList', function (req, res, next) {
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var WishlistModel = require('*/cartridge/models/productList');
    var wishlistModel = new WishlistModel(
        list,
        {
            type: 'wishlist',
            publicView: req.querystring.publicView || false,
            pageSize: PAGE_SIZE_ITEMS,
            pageNumber: req.querystring.pageNumber || 1
        }
    ).productList;
    res.render('/product/components/miniWishlist', { quantityTotal: wishlistModel.length });
    next();
});

server.get('MiniWishlist', function (req, res, next) {
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var WishlistModel = require('*/cartridge/models/productList');
    var wishlistModel = new WishlistModel(
     list,
     {
         type: 'wishlist',
         publicView: req.querystring.publicView || false,
         pageSize: PAGE_SIZE_ITEMS,
         pageNumber: req.querystring.pageNumber || 1
     }
    ).productList;
    res.render('/wishlist/components/miniWishlist', { wishlist: wishlistModel });
    next();
});

module.exports = server.exports();