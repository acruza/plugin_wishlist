'use strict';

var processInclude = require('base/util');

module.exports = function () {

       $('.miniwishlist').on('count:update', function (event, count) {
        if (count && $.isNumeric(count.quantityTotal)) {
            if (count.quantityTotal !== 0) {
                $('.miniwishlist-total').addClass('has-item');
            }
            $('.miniwishlist .miniwishlist-quantity').text(count.quantityTotal);
        }
    });

    $('.miniwishlist').on('mouseenter focusin touchstart', function () {
        if ($('.search:visible').length === 0) {
            return;
        }
        var url = $('.miniwishlist').data('action-url');
        var count = parseInt($('.miniwishlist .miniwishlist-quantity').text(), 10);

        if (count !== 0 && $('.miniwishlist .popover.show').length === 0) {
            $('.miniwishlist .popover').addClass('show');
            $('.miniwishlist .popover').spinner().start();
            $.get(url, function (data) {
                $('.miniwishlist .popover').empty();
                $('.miniwishlist .popover').append(data);
                $.spinner().stop();
            });
        }
    });
    $('body').on('touchstart click', function (e) {
        if ($('.miniwishlist').has(e.target).length <= 0) {
            $('.miniwishlist .popover').empty();
            $('.miniwishlist .popover').removeClass('show');
        }
    });
    $('.miniwishlist').on('mouseleave focusout', function (event) {
        if ((event.type === 'focusout' && $('.miniwishlist').has(event.target).length > 0)
            || (event.type === 'mouseleave' && $(event.target).is('.miniwishlist .quantity'))
            || $('body').hasClass('modal-open')) {
            event.stopPropagation();
            return;
        }
        $('.miniwishlist .popover').empty();
        $('.miniwishlist .popover').removeClass('show');
    });
    $('body').on('change', '.miniwishlist .quantity', function () {
        if ($(this).parents('.bonus-product-line-item').length && $('.cart-page').length) {
            location.reload();
        }
    });
};